#Lab. 7: Pruebas y pruebas unitarias

<img src="http://i.imgur.com/D0laI5r.png?1" width="215" height="174">  <img src="http://i.imgur.com/ggDZ3TQ.png" width="215" height="174">  <img src="http://i.imgur.com/V6xFo00.png?1" width="215" height="174">


<p> </p>

Como habrás aprendido en experiencias de laboratorio anteriores, el lograr que un programa compile es solo una pequeña parte de programar. El compilador se encargará de decirte si hubo errores de sintaxis, pero no podrá detectar errores en la lógica del programa. Es muy importante el probar las funciones del programa para validar que producen los resultados correctos y esperados.

Una manera de hacer estas pruebas es verificando partes del programa "a mano". Otra manera es insertando pedazos de código o funciones y probando el programa con conjuntos representativos de datos que verifiquen si el programa va haciendo lo que se supone y te ayude a detectar problemas. La biblioteca `cassert` de C++  tiene funciones para hacer pruebas y en la experiencia de laboratorio de hoy utilizarás una de ellas.



##Objetivos:

1. Validar el funcionamiento de varios programas haciendo pruebas "a mano".
2. Crear pruebas unitarias para validar funciones a partir de su descripción.



##Pre-Lab:

Antes de llegar al laboratorio debes:

1. haber repasado los conceptos básicos relacionados a pruebas y pruebas unitarias.

2. haber repasado el uso de la función `assert` para hacer pruebas.

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 7](http://moodle.ccom.uprrp.edu/mod/quiz/attempt.php?attempt=2134) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


##Sesión de laboratorio:

###Ejercicio 1

Este ejercicio es una adaptación del ejercicio en [1]. Se te ha dado un programa que implementa varias versiones de cinco funciones simples, por ejemplo, el juego "piedra-papel-tijera" y el ordenar 3 números. Probando las versiones de cada función, tú y tu pareja determinarán cuál de las versiones está  implementada correctamente.

Este ejercicion **NO requiere programación**, solo pruebas "a mano". Para este ejercicio no tienes que implementar las pruebas unitarias, solo usa tu habilidad de hacer pruebas a mano.

Por ejemplo, asumamos que una amiga te provee un porgrama. Ella asegura que el programa resuleve el siguiente problema

    "dados tres enteros, despliega el valor máximo".

Supongamos que el programa tienen una interfase como la siguiente:


![](http://i.imgur.com/BD6SmKh.png)

**Figura 1** - Interfase de un programa para hallar el valor máximo entre tres enteros.

Podrías determinar si el programa provee resultados válidos **sin analizar el código fuente**. Por ejemplo, podrías intentar los siguientes casos:

* a = 4, b = 2, c = 1; resultado esperado: 4
* a = 3, b = 6, c = 2; resultado esperado: 6
* a = 1, b = 10, c = 100; resultado esperado: 100

Si alguno de estos tres casos no da el resultado esperado, el programa de tu amiga no funciona. Por otro lado, si los tres casos funcionan, entonces el programa tiene una probabilidad alta de estar correcto.

En este ejercicio estarás validando varias versiones de las siguientes funciones:


* **3 Sorts:** una función que recibe tres "strings" o números y los ordena en orden lexicográfico (alfabético). Por ejemplo, dados `jirafa`, `zorra`, y `coqui`, los ordena como: `coqui`, `jirafa`, y `zorra`. Para simplificar el ejercicio, solo usaremos "strings" con letras minúsculas. Otro ejemplo, dados `1`, `3` y `2`, los ordena como  `1`, `2`, y `3`.

    ![](http://i.imgur.com/FZkbLSJ.png)

    **Figura 2** - Interfase de la función `3 Sorts`.





* **Dice:** cuando el usuario marca el botón `Roll them`, el programa genera dos números aleatorios entre 1 y 6. El programa informa la suma de los números aleatorios. Tu tarea es determinar en cuál de las versiones (si alguna) la programadora implementó bien la función de suma.

 ![](http://i.imgur.com/Lxt1oVT.png)

    **Figura 3** - Interfase de la función `Dice`.

* **Rock, Papers, Scissors:** Cada uno de los jugadores entra su jugada y el programa informa quién ganó. La interfase del juego se muestra en la siguiente figura:

 ![](http://i.imgur.com/BbdPPwA.png)

    **Figura 4** - Interfase de la función `Rock, Papers, Scissors`.


* **Zulu time:** Dada una hora en tiempo Zulu (Hora en el Meridiano de Greenwich) y la zona militar en la que el usuario desea saber la hora, el programa escribe la hora en esa zona. El formato para el dato de entrada es en formato de 23 horas `####`, por ejemplo `2212` sería las 10:12 pm. La lista de zonas militares válidas la puedes encontrar en  http://en.wikipedia.org/wiki/List_of_military_time_zones. Lo que sigue son ejemplos de cómo deben ser los resultados del programa:

  * Dada hora Zulu 1230 y zona A (UTC+1), el resultado debe ser 1330.
  * Dada hora Zulu 1230 y zona N (UTC-1), el resultado debe ser 1130.
  * Puerto Rico está en la zona militar Q (UTC-4), por lo tanto, cuando es 1800 en hora Zulu, son las 1400 en Puerto Rico.
  
    ![](http://i.imgur.com/85dRODC.png)

    **Figura 5** - Interfase de la función `Zulu time`.

####**Instrucciones:**

1.  Por favor, asegúrate de que tú y tu apreja de laboratorio entienden las descripciones de las funciones de arriba. Pregúntale a tu instructor de laboratorio si necesitas aclarar algo. Para cada una de las funciones descritas arriba, escribe en tu libreta las pruebas que harás para determinar la validez de las funciones. Para cada función, piensa en los errores lógicos que la programadora pudo haber cometido y escribe una prueba que determine si se cometió ese error. Para cada prueba, escribe los valores que utilizarás y el resultado que esperas obtener.


2. Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab07-testing.git` para descargar la carpeta `Lab07-Testing` a tu computadora.

3.  Haz doble "click" en el archivo `testing.pro` para cargar este proyecto a Qt. 

4. Corre el programa. Verás una pantalla similar a la siguiente:

     ![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/ibGORt4.png)

    **Figura 6** - Pantalla para seleccionar la función que se va a probar.

5. Selecciona el botón de `3 Sorts` y obtendrás la siguiente interfase:


     ![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/p1NLIdM.png)

    **Figura 7** - Interfase de `3 Sorts`.


6. La "Version Alpha" en la caja indica que estás corriendo la primera versión del algoritmo `3 Sorts`. Usa las pruebas que escibiste en el paso 1 para validar la "Version Alpha". Luego, haz lo mismo para las versiones Beta, Gamma y Delta. Escribe cuál es la versión correcta en cada una de las funciones. Además, explica las pruebas que realizaste y que te permitieron determinar que las otras versiones son incorrectas.

Por ejemplo, puedes organizar tus respuestas en una tabla como la que sigue:


| 3 Sorts |  |  |  |  |  | 
| ------------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| Num | Test | Result Alpha | Res. Beta | Res. Gamma | Res. Delta | 
| 1| "alce", "coyote", "zorro" | "alce", "coyote", "zorro" | .... | .... |
| 2| "alce", "zorro", "coyote" | "zorro", "alce", "coyote" | .... | .... |


**Figura 8** - Tabla para organizar resultados de las pruebas.

Recuerda añadir información que indique cuál(es) de las versiones fueron implementadas correctamente.  También recuerda especificar cuáles pruebas te permitieron determinar las versiones que eran incorrectas. **Nota:** en cada una de las funciones, puede que ninguna versión sea correcta y puede que haya más de una versión correcta.

Puedes ver ejemplos de cómo organizar tus resultados [aquí](http://i.imgur.com/ggDZ3TQ.png) y [aquí](http://i.imgur.com/rpApVqm.png). Entrega los resultados de este ejercicio en la sección de [Entregas del Lab 7 en Moodle](http://moodle.ccom.uprrp.edu/mod/quiz/edit.php?cmid=7145).


###Ejercicio 2

Hacer pruebas "a mano" cada vez que corres un programa es una tarea que resulta "cansona" bien rápido. Tú y tu pareja lo hicieron para unas pocas funciones simples. !Imagínate hacer lo mismo para un programa complejo completo como un buscador o un procesador de palabras!

Las *pruebas unitarias* ayudan a los programadores a validar códigos y simplificar el proceso de depuración ("debugging") a la vez que evitan la tarea tediosa de hacer pruebas a mano en cada ejecución.

####**Instrucciones:**

1. En el menú de QT, ve a `Build` y selecciona `Clean Project "Testing"`.

2. Ve a la carpeta  `Lab07-Testing` y haz doble "click" en el archivo `UnitTests.pro` para cargar este proyecto a Qt. 

3. El proyecto solo contiene el archivo de código fuente `main.cpp`. Este archivo contiene cuatro funciones: `fact`, `isALetter`, `isValidTime`, y `gcd`, cuyos resultados son solo parcialmente correctos. Tu tarea es escribir pruebas unitarias para cada una de las funciones para identificar los resultados erróneos. ** No necesitas reescribir las funciones para corregirlas. **

  Para la función `fact` se provee la función  `test_fact()`  como función de prueba unitaria. Si invocas esta función desde `main`, compilas y corres el programa debes obtener un mensaje como el siguiente:


  ```cpp
  Assertion failed: (fact(2) == 2), function test_fact, 

  file ../UnitTests/main.cpp, line 69.
  ``` 

  Esto debe ser suficiente para saber que la función `fact` NO está correctamente implementada. Comenta la invocación de `test_fact()` en `main`.


4. Escribe una prueba unitaria llamada `test_isALetter` para la función `isALetter`. En la prueba unitaria escribe varias  afirmaciones ("asserts") para probar algunos datos de entrada y sus valores esperados (mira la función `test_fact` para que te inspires). Invoca `test_isALetter` desde `main`. Continúa escribiendo "asserts" hasta que alguno falle. Copia la prueba unitaria `test_isALetter` y el mensaje de afirmación obtenido en un archivo.

5. Repite el paso 4 paras las otras dos funciones, `isValidTime` y `gcd`.  Recuerda que debes llamar a cada una de las funciones de prueba unitaria desde `main` para que corran. Copia cada una de las funciones y los mensajes de afirmación obtenidos en el archivo y entrega el archivo [aquí] 
(http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7546).



## Referencias

[1] http://nifty.stanford.edu/2005/TestMe/

